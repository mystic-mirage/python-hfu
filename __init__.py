#!/usr/bin/python
# -*- coding: utf-8 -*-

import urllib2, json
from datetime import date
import logging

class HFU():

    host = 'home.finance.ua/services/index.php'
    id = 0
    currency = ('?', 'UAH', 'USD', 'EUR', 'RUB')

    secure = 1
    reauth = False

    def __init__(self, email = '', password = ''):
        self.log = logging.getLogger(__name__)
        self.operations = []
        self.email = email
        self.password = password

    def request(self, service, method, params):
        if self.secure == 0:
            protocols = ('http',)
        if self.secure == 1:
            protocols = ('https',)
        if self.secure == 2:
            protocols = ('https', 'http')
        for p in protocols:
            self.__class__.id += 1
            data = {'id': self.__class__.id, 'service': service,
            'method': method, 'params': params}
            data_json = json.dumps(data)
            self.log.debug(data_json)
            req = urllib2.Request(p + '://' + self.host, data_json,
            {'content-type': 'application/json'})
            response_stream = urllib2.urlopen(req)
            response_json = response_stream.read()
            self.log.debug(response_json)
            response = json.loads(response_json)
            if 'error' in response.keys():
                self.error = response['error']
                if self.error['code'] == 52:
                    continue
                else:
                    break
            else:
                return response['result']
        raise Exception(self.errmsg())

    def query(self, service, method, params):
        ntries = 1
        if self.reauth:
            ntries = 2
        while ntries:
            try:
                result = self.request(service, method,
                [{'token': self.token, 'idUser': self.idUser,
                'idAccount': self.idAccount}, params])
                return result
            except Exception as e:
                self.log.error(e)
                if self.error['code'] == 50 and ntries > 1:
                    self.re_auth()
                else:
                    raise Exception(self.errmsg())
            ntries -= 1

    def __setattrs(self, result):
        for k, v in result.iteritems():
            setattr(self, k, v)

    def errmsg(self):
        emsg = [repr(self.error['code'])]
        if self.error['message']:
            emsg.append(self.error['message'])
        return ': '.join(emsg)

    def auth(self):
        result = self.request('sauth', 'auth', [{'email': self.email,
        'password': self.password}])
        self.__setattrs(result)
        return result

    def re_auth(self):
        result = self.request('sauth', 'auth', [{'email': self.email,
        'password': self.password}])
        self.token = result['token']
        return self.token

    def get_usersettings(self):
        temp, self.idAccount = self.idAccount, self.idUser
        result = self.query('ssettings', 'read', ['userSettings'])
        self.idAccount = temp
        self.__setattrs(result['userSettings'])
        return result['userSettings']

    def get_commonsettings(self):
        result = self.query('ssettings', 'read', ['commonSettings'])
        self.__setattrs(result['commonSettings'])
        return result['commonSettings']

    def get_useraccounts(self):
        temp, self.idAccount = self.idAccount, self.idUser
        result = self.query('ssettings', 'read', ['userAccounts'])
        self.idAccount = temp
        self.userAccounts = result['userAccounts']
        return self.userAccounts

    def get_ctgs(self):
        result = self.query('ssettings', 'read', ['ctgs'])
        self.__setattrs(result['ctgs'])
        return result['ctgs']

    def get_scheta(self):
        result = self.query('ssettings', 'read', ['scheta'])
        self.scheta = result['scheta']
        return self.scheta

    def get_summaryblocks(self):
        result = self.query('ssettings', 'read', ['summaryBlocks'])
        self.summaryBlocks = result['summaryBlocks']
        return self.summaryBlocks

    def get_fintargets(self):
        result = self.query('ssettings', 'read', ['finTargets'])
        self.finTargets = result['finTargets']
        return self.finTargets

    def get_accountdate(self):
        temp, self.idAccount = self.idAccount, self.idUser
        result = self.query('ssettings', 'read', ['accountDate'])
        self.accountDate = result['accountDate']
        self.idAccount = temp
        return result

    def hfudate(self, dt):
        return (dt - date(1970, 1, 1)).days * 86400

    def read_operations(self, f_date, t_date, today = date.today()):
        result = self.query('soperations', 'read',
        {'fromdate': self.hfudate(f_date),
        'todate': self.hfudate(t_date), 'today': self.hfudate(today)})
        return result

    def operation(self, action, otype, date, comment, osum, cur, ctg,
    schet):
        oid = ~ len(self.operations)
        self.operations.append({'operationActionType': action,
        'operationObject': {'id': oid, 'type': otype,
        'date': self.hfudate(date), 'comment': comment, 'sum': osum,
        'idCurrency': cur, 'idCtg': ctg, 'idSchet': schet}})
        return oid

    def transfer(self, action, date, comment, osumf, curf, schetf,
    osumt, curt, schett):
        oid = ~ len(self.operations)
        self.operations.append({'operationActionType': action,
        'operationObject': {'id': oid, 'type': 2,
        'date': self.hfudate(date), 'comment': comment,
        'sumFrom': osumf, 'idCurrencyFrom': curf,
        'idSchetFrom': schetf, 'sumTo': osumt, 'idCurrencyTo': curt,
        'idSchetTo': schett}})
        return oid

    def write_operations(self):
        result = self.query('soperations', 'write', self.operations)
        self.operations = []
        return result

    def add_cost(self, osum, comment = '', ctg = 0, schet = 0,
    cur = 1, date = date.today()):
        return self.operation(0, 0, date, comment, osum, cur, ctg,
        schet)

    def add_revenue(self, osum, comment = '', ctg = 0, schet = 0,
    cur = 1, date = date.today()):
        return self.operation(0, 1, date, comment, osum, cur, ctg,
        schet)

    def add_transfer(self, osumf, osumt, comment = '', schetf = 0,
    schett = 0, curf = 1, curt = 1, date = date.today()):
        return self.transfer(0, date, comment, osumf, curf, schetf,
        osumt, curt, schett)
